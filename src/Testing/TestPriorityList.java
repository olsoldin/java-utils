package Testing;

import Lists.PriorityList;
import org.junit.*;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author Oliver
 */
public class TestPriorityList{

    public static void main(String[] args){
		testCount();
    }
	
    @Test
    public static void testCount(){
    	PriorityList<Object> list = new PriorityList<>();
		list.enqueue(43, 2f, 3f);
    	assertEquals(1, list.count());
		list.enqueue(234, 1f, 1f);
		assertEquals(2, list.count());
		list.dequeueA();
		assertEquals(1, list.count());
		list.clear();
		assertEquals(0, list.count());
    }
}
