package Testing;

import Lists.QueuedStack;
import java.util.Random;

/**
 *
 * @author Oliver
 */
public class TestQueuedStack{

    public static void main(String[] args){
        QueuedStack<Integer> qs;
        QueuedStack<Integer> original = new QueuedStack();

        double fastest = Double.MAX_VALUE;
        String fast = "";
        reset(original);
        
        
        
        qs = new QueuedStack<>(original);
        double bubble = qs.sortBubble();
        if(bubble < fastest){
            fastest = bubble;
            fast = "Bubble";
        }
        System.out.println("Bubble at " + bubble / 1_000_000_000 + "s");

        
        qs = new QueuedStack<>(original);
        double heap = qs.sortHeap();
        if(heap < fastest){
            fastest = heap;
            fast = "Heap";
        }
        System.out.println("Heap at " + heap / 1_000_000_000 + "s");

        
        qs = new QueuedStack<>(original);
        double insertion = qs.sortInsertion();
        if(insertion < fastest){
            fastest = insertion;
            fast = "Insertion";
        }
        System.out.println("Insertion at " + insertion / 1_000_000_000 + "s");

        
        qs = new QueuedStack<>(original);
        double merge = qs.sortMerge();
        if(merge < fastest){
            fastest = merge;
            fast = "Merge";
        }
        System.out.println("Merge at " + merge / 1_000_000_000 + "s");

        
        qs = new QueuedStack<>(original);
        double quick = qs.sortQuick();
        if(quick < fastest){
            fastest = quick;
            fast = "Quick";
        }
        System.out.println("Quick at " + quick / 1_000_000_000 + "s");

        
        qs = new QueuedStack<>(original);
        double selection = qs.sortSelection();
        if(selection < fastest){
            fastest = selection;
            fast = "Selection";
        }
        System.out.println("Selection at " + selection / 1_000_000_000 + "s");

        
        //Extremely slow
//        qs = new QueuedStack<>(original);
//        double stooge = qs.sortStooge();
//        if(stooge < fastest){
//            fastest = stooge;
//            fast = "Stooge";
//        }
//        System.out.println("Stooge at " + stooge / 1_000_000_000 / 60 + "mins");

        System.out.println("Fastest: " + fast + " sort at: " + fastest / 1_000_000_000 + "s");
    }


    private static void reset(QueuedStack qs){
        int max = 10_000;
//        int max = 10;
        Random rnd = new Random();
        for(int i = 0; i < max; i++){
            qs.addTop(rnd.nextInt(max));
        }
    }
}
