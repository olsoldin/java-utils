package Logger;

/**
 *
 * @author Oliver
 */
public class Logger{

    private LogLevel currentLevel = LogLevel.OFF;


    public void setLogLevel(LogLevel level){
        currentLevel = level;
    }


    public final void LOG(String message, Object... parameters){
        logMessage(LogLevel.ALL, message, parameters);
    }


    public final void TRACE(String message, Object... parameters){
        logMessage(LogLevel.TRACE, message, parameters);
    }


    public final void DEBUG(String message, Object... parameters){
        logMessage(LogLevel.DEBUG, message, parameters);
    }


    public final void WARN(String message, Object... parameters){
        logMessage(LogLevel.WARN, message, parameters);
    }


    public final void FATAL(String message, Object... paramters){
        logMessage(LogLevel.FATAL, message, paramters);
    }


    private void logMessage(LogLevel level, String message, Object... parameters){
        if(currentLevel.getLevel() > level.getLevel()){
            //TODO: log the message
            //TODO: format {} into parameters
        }
    }

    private static final Logger INSTANCE = new Logger();


    public static Logger getLogger(Class clazz){
        //TODO: make .LOG .WARN etc static, and store stuff
        // like filename and class called from in an instance
        return INSTANCE;
    }

    public enum LogLevel{

        ALL(5), TRACE(4), DEBUG(3), WARN(2), FATAL(1), OFF(0);

        private int level;


        LogLevel(int level){
            this.level = level;
        }

		/**
		 * @return the level
		 */
		public int getLevel() {
			return level;
		}

		/**
		 * @param level the level to set
		 */
		public void setLevel(int level) {
			this.level = level;
		}
    }

}
