package GUIElements;

import java.awt.Color;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * An extension of the default JTextField, you can add a
 * regex filter, and if the textfields text doesn't match,
 * the text will be red, otherwise it will be green
 *
 * @author Oliver
 */
public class JRegexField extends JTextField{

	private static final long serialVersionUID = 514987484L;

    private final DList DOC_LISTENER = new DList();
    //Less harsh colours
    public static final Color RED = new Color(0xA60000);
    public static final Color GREEN = new Color(0x008500);
    private Color notValidColour = RED;
    private Color validColour = GREEN;

    /**
     * Official RFC valid email regex
     */
    public static final String REGEX_EMAIL = "( ?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    /**
     * REGEX_DATE is the UK standard date (dd/mm/yyyy)
     */
    public static final String REGEX_DATE = "(0?[1-9]|[12][0-9]|3[01])[- /.](0?[1-9]|1[012])[- /.](\\d{4}|\\d{2})\\b";
    /**
     * REGEX_CURRENCY is any number, to a maximum of 2 dp
     * and it can start with £, $, € or nothing
     */
    public static final String REGEX_CURRENCY = "(£|\\$|€|)\\d*(\\.\\d{0,2})?";
    /**
     * Matches all inputs
     */
    public static final String REGEX_ALL = ".*";

    //default regex, accept everything
    private String regexCheck = REGEX_ALL;


    /**
     * Returns the regex used to match the
     * text as a string
     *
     * @return the regex filter
     */
    public String getRegexFilter(){
        return String.valueOf(getRegexCheck());
    }


    /**
     * Sets the filter for the text box, if
     * invalid regex is passed, it defaults
     * to REGEX_ALL and matches all inputs
     *
     * @param regex Regex to filter the
     *              text to
     */
    public void setRegexFilter(String regex){
        try{
            Pattern.compile(regex);
        }catch(PatternSyntaxException e){
            regex = REGEX_ALL;
        }
        setRegexCheck(regex);
        getDocument().addDocumentListener(DOC_LISTENER);
        checkMatches();
    }


    /**
     * Sets the regex to match all inputs, and sets the text colour
     * back to black
     */
    public void removeRegexFilter(){
        setRegexCheck(REGEX_ALL);
        getDocument().removeDocumentListener(DOC_LISTENER);
        setForeground(Color.BLACK);
    }


    /**
     * checks if the text in the textbox matches the regex filter
     * <p>
     * @return true if the text is valid
     */
    public boolean isTextValid(){
        return getText().matches(getRegexCheck());
    }


    /**
     * Cheacks if <b>str</b> matches the regex filter
     * <p>
     * @param str the text to check
     * <p>
     * @return true if <b>str</b> is valid
     */
    public boolean isTextValid(String str){
        return str.matches(getRegexCheck());
    }


    /**
     * Sets the colour the text will be if it doesn't
     * magtch the regex filter
     * <p>
     * @param colour the colour for invalid text
     */
    public void setNonMatchColor(Color colour){
        this.notValidColour = colour;
    }


    /**
     * Sets the colour of the text if it matches
     * the regex filter
     * <p>
     * @param colour the colour for valid text
     */
    public void setMatchColor(Color colour){
        this.validColour = colour;
    }


    /**
     * Checks if the text in the textbox matches the regex
     * filter, and colours it accordingly
     */
    private void checkMatches(){
        setForeground(isTextValid() ? validColour : notValidColour);
    }

    /**
     * A simple document listener to check the validity of all input text
     * and change the colours accordingly, this has to be done with a
     * listener because if you check it while its being inserted it will
     * always check whats already there, instead of checking whats being
     * inserted, even if you try to check it after calling the super method
     * having a document listener will only ever be updated after a change
     * has been made
     */
    private class DList implements DocumentListener{

        @Override
        public void insertUpdate(DocumentEvent e){
            checkMatches();
        }


        @Override
        public void removeUpdate(DocumentEvent e){
            checkMatches();
        }


        @Override
        public void changedUpdate(DocumentEvent e){
            checkMatches();
        }
    }

	/**
	 * @return the regexCheck
	 */
	public String getRegexCheck() {
		return regexCheck;
	}

	/**
	 * @param regexCheck the regexCheck to set
	 */
	public void setRegexCheck(String regexCheck) {
		this.regexCheck = regexCheck;
	}
}
