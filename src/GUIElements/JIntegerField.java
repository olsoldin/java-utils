package GUIElements;

import java.awt.Toolkit;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Oliver
 */
public class JIntegerField extends JTextField {

	private static final long serialVersionUID = 1566848L;

	/**
	 * Returns the int value of the field, if the field is empty, 0 is returned
	 *
	 * @return the value of the field, or 0 if empty
	 */
	public int getInt() {
		final String text = getText();
		if (text == null || text.length() == 0) {
			return 0;
		}
		return Integer.parseInt(text);
	}

	/**
	 * Sets the value of the textbox to the given int value
	 *
	 * @param value the new value of the field
	 */
	public void setInt(int value) {
		setText(String.valueOf(value));
	}

	@Override
	protected Document createDefaultModel() {
		return new IntegerDocument();
	}

}
