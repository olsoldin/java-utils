/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIElements;

import java.awt.Toolkit;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Oliver
 */
class IntegerDocument extends PlainDocument {
	
	private static final long serialVersionUID = 87845L;

	/**
	 * This function gets called when the textbox is updated. It casts the
	 * value to an integer, if there's an exception, nothing is inserted and
	 * the device makes a 'beep' noise. If there are no exeptions, the
	 * string value is passed to the overridden function and inserted
	 * normally.
	 *
	 * @param offs the starting offset >= 0
	 * @param str the string to insert; does nothing with null/empty strings
	 * and beeps with non-integer strings
	 * @param a the attributes for the inserted content
	 *
	 * @throws BadLocationException the given insert position is not a valid
	 * position within the document
	 */
	@Override
	public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
		if (str != null) {
			try {
				Integer.decode(str);
				super.insertString(offs, str, a);
			} catch (NumberFormatException e) {
				Toolkit.getDefaultToolkit().beep();
			}
		}
	}
	
}
