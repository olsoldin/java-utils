package Lists;

import java.util.ArrayList;

/**
 * A simple priority list with 2 priorities
 * 
 * @author Oliver
 * @param <E> Any type to make a list of
 */
public final class PriorityList<E>{

	/**
	 * Stores 3 values in order, (E, float, float)
	 * @param <E> The type of the first element (must extend Object)
	 */
	private class Tuple<E>{

		protected final E value;
		protected final float priorityA;
		protected final float priorityB;


		public Tuple(E value, float priorityA, float priorityB){
			this.value = value;
			this.priorityA = priorityA;
			this.priorityB = priorityB;
		}


		@Override
		public String toString(){
			StringBuilder sb = new StringBuilder(30);
			sb.append("Value=");
			sb.append(value);
			sb.append(" PriorityA=");
			sb.append(priorityA);
			sb.append(" PriorityB=");
			sb.append(priorityB);
			sb.append("\n");
			return sb.toString();
		}
	}

	private final ArrayList<Tuple> queue = new ArrayList<>(1);

	public PriorityList(){
		// Do nothing
	}

	public PriorityList(E value, float priorityA, float priorityB){
		enqueue(value, priorityA, priorityB);
	}

	public void enqueue(E value, float priorityA, float priorityB){
		queue.add(new Tuple(value, priorityA, priorityB));
	}


	public E dequeueA(){
		Tuple<E> max = queue.get(0);
		for(Tuple element : queue){
			if(element.priorityA > max.priorityA){
				max = element;
			}
		}
		queue.remove(max);
		return max.value;
	}


	public E dequeueB(){
		Tuple<E> max = queue.get(0);
		for(Tuple element : queue){
			if(element.priorityB > max.priorityB){
				max = element;
			}
		}
		queue.remove(max);
		return max.value;
	}


	public E dequeueFirst(){
		Tuple<E> first = queue.get(0);
		queue.remove(first);
		return first.value;
	}


	public int count(){
		return queue.size();
	}


	public void clear(){
		queue.clear();
	}


	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder(1);
		queue.forEach(sb::append);
		return sb.toString();
	}
}
