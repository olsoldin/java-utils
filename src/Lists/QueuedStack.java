package Lists;

import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.Random;

/**
 * A class that you can use to store data in a list, or as a queue including
 * built in methods to sort data, if that data extends the comparable class.
 * <p>
 * @author Oliver
 * @param <E> if E extends comparable, it can be sorted
 */
public class QueuedStack<E> implements Iterable<E> {

	private E[] stack;

	//@TODO: Increase efficiency of sorting
	/**
	 * Creates a new Stack with nothing in it
	 */
	public QueuedStack() {
		this(0);
	}

	/**
	 * Creates an empty stack
	 * <p>
	 * @param size the size of the empty stack
	 */
	public QueuedStack(int size) {
		stack = (E[]) new Object[size];
	}

	/**
	 * Creates a copy of the passed stack
	 * <p>
	 * @param qs the Stack to copy
	 */
	public QueuedStack(QueuedStack<E> qs) {
		if (qs.getArr() == null) {
			stack = (E[]) new Object[0];
		}
		stack = Arrays.copyOf(qs.getArr(), qs.size());
	}

	/**
	 * Creates a new Stack with the E array as the values
	 * <p>
	 * @param stack the values of the stack
	 */
	public QueuedStack(E[] stack) {
		this.stack = Arrays.copyOf(stack, stack.length);
	}

	/**
	 * Returns whether the Stack contains the given object or not
	 * <p>
	 * @param o the E to check
	 * <p>
	 * @return true if the stack contains o
	 */
	public boolean contains(E o) {
		for (E e : stack) {
			if (e.equals(o)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the number of elements in the Stack
	 * <p>
	 * @return number of elements in the Stack
	 */
	public int size() {
		return stack.length;
	}

	/**
	 * Returns <b>true</b> if this list contains no elements.
	 * <p>
	 * @return <b>true</b> if this list contains no elements
	 */
	public boolean isEmpty() {
		return size() == 0;
	}

	/**
	 * Adds the object to the top of the Stack
	 * <p>
	 * @param o added to the top of the stack
	 */
	public void addTop(E o) {
		Object[] temp = new Object[stack.length + 1];
		System.arraycopy(stack, 0, temp, 0, stack.length);
		stack = (E[]) temp;
		stack[stack.length - 1] = o;
	}

	/**
	 * Adds the stack to the top
	 * <p>
	 * @param qs the stack to be appended to the top
	 */
	public void addTop(QueuedStack<E> qs) {
		addTop(Arrays.copyOf(qs.getArr(), qs.size()));
	}

	/**
	 * Adds the array to the top of the stack, added in order in the array
	 * <p>
	 * @param o the array to add to the Stack
	 */
	public void addTop(E[] o) {
		for (E e : o) {
			addTop(e);
		}
	}

	/**
	 * Adds the stack to the bottom
	 * <p>
	 * @param qs the stack to be added
	 */
	public void addBottom(QueuedStack<E> qs) {
		addBottom(Arrays.copyOf(qs.getArr(), qs.size()));
	}

	/**
	 * Adds the E to the bottom of the Stack
	 * <p>
	 * @param o the value to be added
	 */
	public void addBottom(E o) {
		Object[] temp = new Object[stack.length + 1];
		System.arraycopy(stack, 0, temp, 1, stack.length);
		stack = (E[]) temp;
		stack[0] = o;
	}

	/**
	 * Adds the array to the bottom of the stack, in the order its given in
	 * <p>
	 * @param o the array to add to the beginning of the stack
	 */
	public void addBottom(E[] o) {
		//no way to do reverse for:each loop
		for (int i = o.length - 1; i >= 0; i--) {
			addBottom(o[i]);
		}
	}

	/**
	 * Gets the element of the Stack at index Removes the element from the Stack
	 * <p>
	 * @param index the index of the element to remove
	 * <p>
	 * @return the element at index
	 */
	public E pop(int index) {
		checkBounds(index);
		Object element = stack[index];
		Object[] temp = new Object[stack.length - 1];
		System.arraycopy(stack, 0, temp, 0, index);
		System.arraycopy(stack, index + 1, temp, index, stack.length - index - 1);
		stack = (E[]) temp;
		return (E) element;
	}

	/**
	 * Gets the top element of the Stack Removes the element from the Stack
	 * <p>
	 * @return the top element
	 */
	public E popTop() {
		checkStack();
		Object[] temp = new Object[stack.length - 1];
		System.arraycopy(stack, 0, temp, 0, temp.length);
		E obj = stack[stack.length - 1];
		stack = (E[]) temp;
		return obj;
	}

	/**
	 * Returns the top "num" amount of items in the stack, will return null if
	 * the number is out of boundss Removes the elements from the stack.
	 * <p>
	 * @param num the number of elements to pop from the top
	 * <p>
	 * @return the popped elements
	 */
	public E[] popTop(int num) {
		checkBounds(num);
		Object[] temp = new Object[num];
		for (int i = num - 1; i >= 0; i--) {
			temp[i] = popTop();
		}
		return (E[]) temp;
	}

	/**
	 * Returns the bottom element of the Stack Removes the element from the
	 * Stack
	 * <p>
	 * @return the bottom element of the stack
	 */
	public E popBottom() {
		if (stack.length < 1) {
			throw new EmptyStackException();
		}
		Object[] temp = new Object[stack.length - 1];
		System.arraycopy(stack, 1, temp, 0, temp.length);
		E obj = stack[0];
		stack = (E[]) temp;
		return obj;
	}

	/**
	 * Returns the bottom "num" amount of elements Removes them from the Stack
	 * <p>
	 * @param num the amount of elements to return
	 * <p>
	 * @return the objects in an array from
	 */
	public E[] popBottom(int num) {
		checkBounds(num);
		Object[] temp = new Object[num];
		for (int i = 0; i < num; i++) {
			temp[i] = popBottom();
		}
		return (E[]) temp;
	}

	/**
	 * Returns the whole stack as an array Removes all elements from the stack
	 * <p>
	 * @return the objects in an array from
	 */
	public E[] popAllTop() {
		return popTop(stack.length);
	}

	/**
	 * Returns the whole stack as an array Removes all elements from the stack
	 * <p>
	 * @return the objects in an array form
	 */
	public E[] popAllBottom() {
		return popBottom(stack.length);
	}

	/**
	 * Returns all the elements of the Stack as a single array
	 * <p>
	 * @return an array of the Stack elements
	 */
	public E[] getArr() {
		return stack;
	}

	/**
	 * Returns the top element of the Stack, without removing it
	 * <p>
	 * @return top element of the Stack
	 */
	public E getTop() {
		return stack[stack.length - 1];
	}

	/**
	 * Returns the top "num" amount of items in the stack, will return null if
	 * the number is out of boundss Doesn't remove the elements from the stack.
	 * <p>
	 * @param num the number of elements to pop from the top
	 * <p>
	 * @return the popped elements
	 */
	public E[] getTop(int num) {
		checkBounds(num);
		Object[] temp = new Object[num];
		for (int i = num - 1; i >= 0; i--) {
			temp[i] = getTop();
		}
		return (E[]) temp;
	}

	/**
	 * Returns the bottom element of the Stack, without removing it
	 * <p>
	 * @return bottom element of the Stack
	 */
	public E getBottom() {
		return stack[0];
	}

	/**
	 * Returns the bottom "num" amount of elements Doesn't remove them from the
	 * Stack
	 * <p>
	 * @param num the amount of elements to return
	 * <p>
	 * @return the objects in an array form
	 */
	public E[] getBottom(int num) {
		checkBounds(num);
		Object[] temp = new Object[num];
		for (int i = 0; i < num; i++) {
			temp[i] = getBottom();
		}
		return (E[]) temp;
	}

	/**
	 * Returns the element at the index position of the stack
	 * <p>
	 * @param index the element to return
	 * <p>
	 * @return the element at the given index
	 */
	public E get(int index) {
		checkBounds(index);
		return stack[index];
	}

	/**
	 * Gets the max value in the stack
	 * <p>
	 * @return the max value
	 */
	public E max() {
		QueuedStack<? extends Comparable> temp = new QueuedStack(this);
		Object max = 0;
		for (int i = 0; i < temp.size(); i++) {
			if (temp.get(i).compareTo(max) > 0) {
				max = temp.get(i);
			}
		}
		return (E) max;
	}

	/**
	 * Gets the min value in the stack
	 * <p>
	 * @return the min value
	 */
	public E min() {
		QueuedStack<? extends Comparable> temp = new QueuedStack(this);
		Object min = Integer.MAX_VALUE;
		for (int i = 0; i < temp.size(); i++) {
			if (temp.get(i).compareTo(min) < 0) {
				min = temp.get(i);
			}
		}
		return (E) min;
	}

	//================================Sorting algorithms========================================
	/**
	 * Sorts the stack using selection sort
	 * <p>
	 * @return the time it takes to sort the data in nano seconds
	 */
	public double sortSelection() {
		QueuedStack<? extends Comparable> temp = new QueuedStack(this);
		double startTime = System.nanoTime();
		//=====================Selection sorting=======================
        /* a[0] to a[n-1] is the array to sort */
		int i, j;
		int iMin;

		/* advance the position through the entire array */
		/*   (could do j < n-1 because single element is also min element) */
		for (j = 0; j < temp.size() - 1; j++) {
			/* find the min element in the unsorted a[j .. n-1] */

			/* assume the min is the first element */
			iMin = j;
			/* test against elements after j to find the smallest */
			for (i = j + 1; i < temp.size(); i++) {
				/* if this element is less, then it is the new minimum */
				if (temp.get(i).compareTo(temp.get(iMin)) < 0) {
					/* found new minimum; remember its index */
					iMin = i;
				}
			}

			/* iMin is the index of the minimum element. Swap it with the current position */
			if (iMin != j) {
				temp.swap(iMin, j);
			}
		}
		//======================================================================
		double endTime = System.nanoTime();
		stack = (E[]) temp.getArr();
		return endTime - startTime;
	}

	/**
	 * Sorts the stack using insertion sort for i ← 1 to length(A) j ← i while j
	 * gt 0 and A[j-1] gt A[j] swap A[j] and A[j-1] j ← j - 1
	 * <p>
	 * @return the time it takes to sort the data in nano seconds
	 * <p>
	 */
	public double sortInsertion() {
		QueuedStack<? extends Comparable> temp = new QueuedStack(this);
		double startTime = System.nanoTime();
		//===================Insertion sort========================
		int i, j;
		for (i = 1; i < temp.size(); i++) {
			j = i;
			while (j > 0 && temp.get(j - 1).compareTo(temp.get(j)) > 0) {
				temp.swap(j - 1, j);
				j--;
			}
		}
		//=========================================================
		double endTime = System.nanoTime();
		stack = (E[]) temp.getArr();
		return endTime - startTime;
	}

	/**
	 * Sorts the stack using bubble sort n = length(A) repeat newn = 0 for i = 1
	 * to n-1 inclusive if A[i-1] gt A[i] then swap(A[i-1], A[i]) newn = i end
	 * if end for n = newn nutil n = 0
	 * <p>
	 *
	 * @return the time it takes to sort the data in nano seconds
	 */
	public double sortBubble() {
		QueuedStack<? extends Comparable> temp = new QueuedStack(this);
		double startTime = System.nanoTime();
		//======================Bubble sort=====================
		int n, newn;
		n = temp.size();
		do {
			newn = 0;
			for (int i = 1; i < n; i++) {
				if (temp.get(i - 1).compareTo(temp.get(i)) > 0) {
					temp.swap(i - 1, i);
					newn = i;
				}
			}
			n = newn;
		} while (n > 0);
		//======================================================
		double endTime = System.nanoTime();
		stack = (E[]) temp.getArr();
		return endTime - startTime;
	}

	/**
	 * Sorts the stack using quick sort
	 * <p>
	 * @return the time it takes to sort the data in nano seconds
	 */
	public double sortQuick() {
		QueuedStack<? extends Comparable> temp = new QueuedStack(this);
		double startTime = System.nanoTime();
		//========================Quick sort=============================
		stack = quickSort(temp);
		//===============================================================
		double endTime = System.nanoTime();
		return endTime - startTime;
	}

	/**
	 * Uses recursion to sort the elements of the array function
	 * quicksort(array) if length(array) ≤ 1 return array // an array of zero or
	 * one elements is already sorted select and remove a pivot element pivot
	 * from 'array' // random position create empty lists less and greater for
	 * each x in array if x ≤ pivot then append x to less else append x to
	 * greater return concatenate(quicksort(less), list(pivot),
	 * quicksort(greater)) // two recursive calls
	 * <p>
	 *
	 * @param intArray the array to sort
	 * <p>
	 * @return the sorted array
	 */
	private E[] quickSort(QueuedStack<? extends Comparable> intArray) {
		if (intArray.size() <= 1) {
			return (E[]) intArray.getArr();
		}
		// TODO: change it to use things like "compere()" so it works with things other than integers
		int pivotPoint = new Random().nextInt(intArray.size());
		QueuedStack less = new QueuedStack();
		Integer pivot = (Integer) intArray.pop(pivotPoint);
		QueuedStack greater = new QueuedStack();

		for (int i = 0; i < intArray.size(); i++) {
			Integer currInteger = (Integer) intArray.get(i);
			if (currInteger <= pivot) {
				less.addTop(currInteger);
			} else {
				greater.addTop(currInteger);
			}
		}
		return concat((E[]) quickSort(less), (E) pivot, (E[]) quickSort(greater));
	}

	/**
	 * Sorts the stack using stooge sort (only currently works for integers)
	 * <p>
	 * @return the time it takes to sort the data in nano seconds
	 */
	public double sortStooge() {
		QueuedStack<? extends Comparable> temp = new QueuedStack(this);
		double startTime = System.nanoTime();
		//========================Stooge sort============================
		stack = stoogeSort(temp, 0, stack.length - 1);
		//===============================================================
		double endTime = System.nanoTime();
		return endTime - startTime;
	}

	/**
	 * Stoogesort is a very slow kind of sort function stoogesort(array L, i =
	 * 0, j = length(L)-1) if L[j] < L[i] then
	 * L[i] ↔ L[j]
	 * if (j - i + 1) >= 3 then t = (j - i + 1) / 3 stoogesort(L, i , j-t)
	 * stoogesort(L, i+t, j ) stoogesort(L, i , j-t) return L
	 * <p>
	 * @param qs the stack to sort
	 * @param i  the starting position of the sort
	 * @param j  the ending position of the sort
	 * <p>
	 * @return the sorted stack as an array
	 */
	private E[] stoogeSort(QueuedStack<? extends Comparable> qs, int i, int j) {
		if (qs.get(j).compareTo(qs.get(i)) < 0) {
			qs.swap(i, j);
		}
		if (j - i + 1 >= 3) {
			int t = (j - i + 1) / 3;
			stoogeSort(qs, i, j - t);
			stoogeSort(qs, i + t, j);
			stoogeSort(qs, i, j - t);
		}
		return (E[]) qs.getArr();
	}

	/**
	 * Sorts the stack using merge sort (only currently works for integers)
	 * <p>
	 * @return the time it takes to sort the data in nano seconds
	 */
	public double sortMerge() {
		QueuedStack<? extends Comparable> temp = new QueuedStack(this);
		double startTime = System.nanoTime();
		//========================Merge sort============================
		stack = mergeSort(temp);
		//===============================================================
		double endTime = System.nanoTime();
		return endTime - startTime;
	}

	/**
	 * Merge sort sorts small sections of the array, then merges them all
	 * together at the end
	 * <p>
	 * function merge_sort(list m) // Base case. A list of zero or one elements
	 * is sorted, by definition. if length(m) <= 1 return m <p>
	 *  // Recursive case. First, *divide* the list into equal-sized sublists.
	 * var list left, right var integer middle = length(m) / 2 for each x in m
	 * before middle add x to left for each x in m after or equal middle add x
	 * to right
	 * <p>
	 *  // Recursively sort both sublists. left = merge_sort(left) right =
	 * merge_sort(right) // *Conquer*: merge the now-sorted sublists. return
	 * merge(left, right) //smallest first
	 * <p>
	 * @param qs the stack to sort
	 * <p>
	 * @return the sorted stack as an array
	 */
	private E[] mergeSort(QueuedStack<? extends Comparable> qs) {
		if (qs.size() <= 1) {
			return (E[]) qs.getArr();
		}
		QueuedStack left = new QueuedStack();
		QueuedStack right = new QueuedStack();
		int middle = qs.size() / 2;
		for (int i = 0; i < middle; i++) {
			left.addTop(qs.get(i));
		}
		for (int i = middle; i < qs.size(); i++) {
			right.addTop(qs.get(i));
		}
		left = new QueuedStack(mergeSort(left));
		right = new QueuedStack(mergeSort(right));
		return (E[]) merge(left, right);
	}

	/**
	 * function merge(left, right) // receive the left and right sublist as
	 * arguments. // 'result' variable for the merged result of two sublists.
	 * var list result // assign the element of the sublists to 'result'
	 * variable until there is no element to merge. while length(left) > 0 or
	 * length(right) > 0 if length(left) > 0 and length(right) > 0 // compare
	 * the first two element, which is the small one, of each two sublists. if
	 * first(left) <= first(right)
	 *               // the small element is copied to 'result' variable.
	 *               // delete the copied one(a first element) in the sublist.
	 * append first(left) to result
	 * left = rest(left)
	 * else
	 *               // same operation as the above(in the right sublist).
	 * append first(right) to result
	 * right = rest(right)
	 * else if length(left) > 0
	 *           // copy all of remaining elements from the sublist to 'result' variable,
	 * // when there is no more element to compare with. append first(left) to
	 * result left = rest(left) else if length(right) > 0 // same operation as
	 * the above(in the right sublist). append first(right) to result right =
	 * rest(right) end while // return the result of the merged sublists(or
	 * completed one, finally). // the length of the left and right sublists
	 * will grow bigger and bigger, after the next call of this function. return
	 * result
	 */
	private E[] merge(QueuedStack<? extends Comparable> left, QueuedStack<? extends Comparable> right) {
		QueuedStack result = new QueuedStack();
		while (left.size() > 0 || right.size() > 0) {
			if (left.size() > 0 && right.size() > 0) {
				if (left.get(0).compareTo(right.get(0)) <= 0) {
					result.addTop(left.popBottom());
				} else {
					result.addTop(right.popBottom());
				}
			} else if (left.size() > 0) {
				result.addTop(left.popAllTop());
			} else if (right.size() > 0) {
				result.addTop(right.popAllTop());
			}
		}
		return (E[]) result.getArr();
	}

	/**
	 * Sorts the stack using heap sort (only currently works for integers)
	 * <p>
	 * @return the time it takes to sort the data in nano seconds
	 */
	public double sortHeap() {
		QueuedStack<? extends Comparable> temp = new QueuedStack(this);
		double startTime = System.nanoTime();
		//========================Heap sort============================
		stack = heapSort(temp);
		//===============================================================
		double endTime = System.nanoTime();
		return endTime - startTime;
	}

	/**
	 * function heapsort(a, count) is input: an unordered array a of length
	 * count
	 * <p>
	 * (: Build the heap in array a so that largest value is at the root :)
	 * heapify(a, count)
	 * <p>
	 * (: The following loop maintains the invariants that a[0:end] is a heap
	 * and every element : beyond end is greater than everything before it (so
	 * a[end:count] is in sorted order). :) end ← count - 1 while end > 0 do (:
	 * a[0] is the root and largest value. The swap moves it in front of the
	 * sorted elements.:) swap(a[end], a[0]) (: the heap size is reduced by one
	 * :) end ← end - 1 (: the swap ruined the heap property, so restore it :)
	 * siftDown(a, 0, end)
	 */
	private E[] heapSort(QueuedStack<? extends Comparable> qs) {
		heapify(qs);
		int end = qs.size() - 1;
		while (end > 0) {
			qs.swap(end, 0);
			end--;
			siftDown(qs, 0, end);
		}
		return (E[]) qs.getArr();
	}

	/**
	 * (: Put elements of a in heap order, in-place :) function heapify(a,
	 * count) is (start is assigned the index in a of the last parent node) (the
	 * last element in a 0-based array is at index count-1; find the root of
	 * that element ) start ← (count - 2 ) / 2
	 * <p>
	 * while start ≥ 0 do (sift down the node at index start to the proper place
	 * such that all nodes below the start index are in heap order) siftDown(a,
	 * start, count-1) (go to the next parent node) start ← start - 1 (after
	 * sifting down the root all nodes/elements are in heap order)
	 */
	private void heapify(QueuedStack<? extends Comparable> qs) {
		int start = (qs.size() - 2) / 2;
		while (start >= 0) {
			siftDown(qs, start, qs.size() - 1);
			start--;
		}
	}

	/**
	 * function siftDown(a, start, end) is root ← start
	 * <p>
	 * while root * 2 + 1 ≤ end do (: While the root has at least one child :)
	 * child ← root * 2 + 1 (: left child :) swap ← root (: keeps track of child
	 * to swap with :)
	 * <p>
	 * if a[swap] < a[child] swap ← child (: if there is a right child and that
	 * child is greater :) if child+1 ≤ end and a[swap] < a[child+1] swap ←
	 * child + 1 if swap ≠ root swap(a[root], a[swap]) root ← swap (: repeat to
	 * continue sifting down the child now :) else return
	 */
	private void siftDown(QueuedStack<? extends Comparable> qs, int start, int end) {
		int root = start;
		while (root * 2 + 1 <= end) {
			int child = root * 2 + 1;
			int swap = root;
			if (qs.get(swap).compareTo(qs.get(child)) < 0) {
				swap = child;
			}
			if (child + 1 <= end && qs.get(swap).compareTo(qs.get(child + 1)) < 0) {
				swap = child + 1;
			}
			if (swap != root) {
				qs.swap(root, swap);
				root = swap;
			} else {
				return;
			}
		}
	}

	//=======================================End Sorting=================================
	/**
	 * Glues 2 arrays together with a single value in between
	 * <p>
	 * @param arr1 first array
	 * @param ell2 middle element
	 * @param arr3 last array
	 * <p>
	 * @return a new array in the form "arr1 + ell2 + arr3"
	 */
	private E[] concat(E[] arr1, E ell2, E[] arr3) {
		//create a new array of the correct size
		Object[] temp = new Object[arr1.length + 1 + arr3.length];
		//copy the arrays to the created one
		System.arraycopy(arr1, 0, temp, 0, arr1.length);
		temp[arr1.length] = ell2;
		System.arraycopy(arr3, 0, temp, arr1.length + 1, arr3.length);
		//return it as a generic type
		return (E[]) temp;
	}

	/**
	 * Throws an empty stack exception if the stack is empty
	 */
	private void checkStack() {
		if (isEmpty()) {
			throw new EmptyStackException();
		}
	}

	/**
	 * Checks that the given number is greater than 0, and less than the length
	 * of the stack. throws an IndexOutOfBoundsException
	 * <p>
	 * @param num the num to check
	 */
	private void checkBounds(int num) {
		checkStack();
		if (num < 0 || num > size()) {
			throw new IndexOutOfBoundsException();
		}
	}

	/**
	 * Swaps element <b>i</b> and <b>j</b> in the stack
	 * <p>
	 * @param i the first element
	 * @param j the second element
	 */
	public void swap(int i, int j) {
		E temp = stack[i];
		stack[i] = stack[j];
		stack[j] = temp;
	}

	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			private int index = 0;

			@Override
			public boolean hasNext() {
				return index < stack.length && stack[index] != null;
			}

			@Override
			public E next() {
				return stack[index++];
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException("Not supported yet.");
			}

		};
	}

	/**
	 * Returns all the elements in the Stack as a list in the form: "Element
	 * 1\n" + "Element 2\n" + "Element 3\n" + ...
	 * <p>
	 * If the Stack is empty, it will return "Empty" All leading and trailing
	 * whitespace is trimmed
	 * <p>
	 * @return A String representation of the elements of the Stack
	 */
	@Override
	public String toString() {
		String str = "";
		if (stack.length == 0) {
			return "Empty";
		}
		for (E e : stack) {
			str += e + "\n";
		}
		return str.trim();
	}

}
